#include "ConstrainedDelaunayPainter.h"
#include "Messenger/Messenger.h"
#include "Windows Input/MouseMessage.h"
#include "Windows Input/WindowsInputMessage.h"

CConstrainedDelaunayPainter::CConstrainedDelaunayPainter( CConstrainedDelaunayTriangulation& rTriangulation )
	: m_rTriangulation( rTriangulation )
	, m_eState( NONE )
{
	CMessenger::GlobalListen( *this, CMouseMessage::GetMouseMessageUID() );
	CMessenger::GlobalListen( *this, CWindowsInputMessage::GetWindowsInputMessageType() );
}

CConstrainedDelaunayPainter::~CConstrainedDelaunayPainter()
{
	CMessenger::GlobalStopListening( *this, CMouseMessage::GetMouseMessageUID() );
	CMessenger::GlobalStopListening( *this, CWindowsInputMessage::GetWindowsInputMessageType() );
}

void CConstrainedDelaunayPainter::Push( CMessage& rMessage )
{
	switch( m_eState )
	{
		case NONE:
		{
			if( rMessage.GetType() == CWindowsInputMessage::GetWindowsInputMessageType() )
			{
				CWindowsInputMessage& rWindowsMessage( static_cast< CWindowsInputMessage& >( rMessage ) );
				if( rWindowsMessage.GetWindowsInputMessage() == WM_KEYDOWN )
				{
					if( rWindowsMessage.GetParameter1() == 'P' )
					{
						if( m_rTriangulation.GetPolygon().size() == 0 )
						{
							m_eState = DRAWING_POLYGON;
						}
					}
					else if( rWindowsMessage.GetParameter1() == 'H' )
					{
						m_eState = DRAWING_HOLE;
					}
					else if( rWindowsMessage.GetParameter1() == 'T' )
					{
						m_rTriangulation.Triangulate();
						m_eState = FINISHED;
					}
					else if( rWindowsMessage.GetParameter1() == 'C' )
					{
						m_rTriangulation.Reset();
					}
				}
			}
			break;
		}
		case DRAWING_POLYGON:
		{
			if( rMessage.GetType() == CMouseMessage::GetMouseMessageUID() )
			{
				CMouseMessage& rMouseMessage( static_cast< CMouseMessage& >( rMessage ) );
				if( rMouseMessage.GetMouseAction() == CMouseMessage::LEFT_BUTTON_CLICK )
				{
					m_rTriangulation.AddPoint( CF32Vector2( rMouseMessage.X(), rMouseMessage.Y() ) );
				}
			}
			else if( rMessage.GetType() == CWindowsInputMessage::GetWindowsInputMessageType() )
			{
				CWindowsInputMessage& rWindowsMessage( static_cast< CWindowsInputMessage& >( rMessage ) );
				if( (rWindowsMessage.GetWindowsInputMessage)() == WM_KEYUP )
				{
					if( rWindowsMessage.GetParameter1() == 'P' )
					{
						if (m_rTriangulation.GetPolygon().size() == 0)
						{
							m_rTriangulation.CreatePolygon();
							m_eState = NONE;
						}
					}
				}
			}
			break;
		}
		case DRAWING_HOLE:
		{
			if( rMessage.GetType() == CMouseMessage::GetMouseMessageUID() )
			{
				CMouseMessage& rMouseMessage( static_cast< CMouseMessage& >( rMessage ) );
				if( rMouseMessage.GetMouseAction() == CMouseMessage::LEFT_BUTTON_CLICK )
				{
					m_rTriangulation.AddPoint( CF32Vector2( rMouseMessage.X(), rMouseMessage.Y() ) );
				}
			}
			else if( rMessage.GetType() == CWindowsInputMessage::GetWindowsInputMessageType() )
			{
				CWindowsInputMessage& rWindowsMessage( static_cast< CWindowsInputMessage& >( rMessage ) );
				if( (rWindowsMessage.GetWindowsInputMessage)() == WM_KEYUP )
				{
					if( rWindowsMessage.GetParameter1() == 'H' )
					{
						m_rTriangulation.CreateHole();
						m_eState = NONE;
					}
				}
			}
			break;
		}
		case FINISHED:
		{
			if( rMessage.GetType() == CWindowsInputMessage::GetWindowsInputMessageType() )
			{
				CWindowsInputMessage& rWindowsMessage( static_cast< CWindowsInputMessage& >( rMessage ) );
				if( rWindowsMessage.GetWindowsInputMessage() == WM_KEYDOWN )
				{
					if( rWindowsMessage.GetParameter1() == 'C' )
					{
						m_rTriangulation.Reset();
						m_eState = NONE;
					}
				}
			}
			break;
		}
	}
}
