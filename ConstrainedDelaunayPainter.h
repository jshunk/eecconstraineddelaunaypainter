#pragma once

#include "Messenger/Listener.h"
#include "Constrained Delaunay Triangulation/ConstrainedDelaunayTriangulation.h"

class CWindow;

class CConstrainedDelaunayPainter : public CListener
{
private:
	enum EConstrainedDelaunayPainterState
	{
		NONE,
		DRAWING_POLYGON,
		DRAWING_HOLE,
		FINISHED
	};

public:
										CConstrainedDelaunayPainter( CConstrainedDelaunayTriangulation& rTriangulation );
										~CConstrainedDelaunayPainter() override;
	void								Push( CMessage& rMessage ) override;
	CConstrainedDelaunayTriangulation&	GetTriangulation() { return m_rTriangulation; }

private:
	CConstrainedDelaunayTriangulation&	m_rTriangulation;
	EConstrainedDelaunayPainterState	m_eState;
};
